
angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})



.config(function($stateProvider,$urlRouterProvider){ 
    $stateProvider
    .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html' 
    })
    
    .state('tab.home', {
        url: '/home',
        views: {
            'tab-home': {
            templateUrl: 'templates/home.html' }
        } 
    })
    .state('tab.repuestos', {
        url: '/repuestos',
        views: {
          'tab-repuestos': {
              templateUrl: 'templates/repuestos.html',
              controller: 'RepCtrl'
          }
        }
    })
    .state('tab.vendedores',{
        url:'/vendedores',
        views:{
            'tab-vendedores':{
                templateUrl:'templates/vendedores.html',
                controller:'VendedorCtrl'
            }
        }
        
    })
    $urlRouterProvider.otherwise('/tab/home'); 
})

.controller('RepCtrl', ['$scope', '$http', '$state', function($scope, $http, $state) {
    $http.get('js/data.json') .success(function(data){
        $scope.detalles = data.detalles; 
    });
}])


.controller('VendedorCtrl', ['$scope', '$http', '$state', function($scope, $http, $state) {
    $http.get('js/data.json').success(function(data){
        $scope.vendedores = data.usuarios;         
    });
}])

